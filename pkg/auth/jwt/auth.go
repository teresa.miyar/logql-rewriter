package jwt

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/logql-rewriter/pkg/jwt"
	"golang.org/x/time/rate"
)

type Verifier interface {
	Verify(ctx context.Context, rawIDToken string) (*oidc.IDToken, error)
}

type cacheEntry struct {
	Verifier  Verifier
	ExpiresAt time.Time
}

const (
	defaultMaxCacheSize = 10000
)

// Authorizer .
type Authorizer struct {
	headerName     string
	jwtFromHeader  string
	clientID       string           // This is the client id we look for when looking up roles in the claim.
	ctx            context.Context  // Close to shutdown (cache cleaning goroutine).
	allowedIssuers []*regexp.Regexp // List of patterns allowing issuers.

	//providers map[string]*oidc.Provider
	mu           sync.Mutex            // Protects the verifier map.
	maxCacheSize int                   // The maximum cache size allowed.
	verifiers    map[string]cacheEntry // Maps issuer URLs to Verifiers.
}

var once sync.Once

// Authorize the JWT against the IdP present in and verify if the claims allow
// the currently set content of headerName (tenant). Requests to the IdP will be
// issued to fetch the current keyset (JWKS) and will be cached by the verifier
// itself.
func (au *Authorizer) Authorize(ctx context.Context, r *http.Request, limit *rate.Limiter) error {
	hdr := r.Header.Get(au.headerName)
	if hdr == "" {
		return fmt.Errorf("header %s is empty", au.headerName)
	}
	accessToken := r.Header.Get(au.jwtFromHeader)
	if accessToken == "" {
		return fmt.Errorf("no JWT found in header %s", au.jwtFromHeader)
	}
	accessToken = strings.TrimPrefix(accessToken, "Bearer ")
	tok, err := jwt.ParseJWT(accessToken)
	if err != nil {
		return fmt.Errorf("could not parse JWT: %w", err)
	}
	if err := jwt.AuthorizeMethod(tok, r.Method, au.clientID); err != nil {
		return fmt.Errorf("could not authorize request: %w", err)
	}
	logrus.Debugf("method %s authorized", r.Method)
	verifier, err := au.getOIDCVerifier(ctx, accessToken)
	if err != nil {
		return fmt.Errorf("error contacting issuer IdP: %w", err)
	}
	token, err := verifier.Verify(ctx, accessToken)
	if err != nil {
		return fmt.Errorf("could not verify access token: %w", err)
	}
	var claims claims
	if err := token.Claims(&claims); err != nil {
		return fmt.Errorf("could not parse tenant claims: %w", err)
	}
	logrus.Debugf("got valid id token from JWT, allowed tenants: %v", append(claims.Tenants, claims.Tenant))
	for _, v := range claims.Tenants {
		if v == hdr {
			return nil // Success!
		}
	}
	if hdr == claims.Tenant {
		return nil // Success!
	}
	return fmt.Errorf("tenant %s not in authorized tenant list", hdr)
}

// ErrCacheOverflow is signaled if the cache is too large to host a new entry.
var ErrCacheOverflow = fmt.Errorf("cache grew too large")

type audience []string

func (a *audience) UnmarshalJSON(b []byte) error {
	var s string
	if json.Unmarshal(b, &s) == nil {
		*a = audience{s}
		return nil
	}
	var auds []string
	if err := json.Unmarshal(b, &auds); err != nil {
		return err
	}
	*a = audience(auds)
	return nil
}

type claims struct {
	Audience        audience `json:"aud"`           // Required field.
	Issuer          string   `json:"iss"`           // Required field.
	AuthorizedParty string   `json:"azp,omitempty"` // https://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation
	Tenant          string   `json:"tenant,omitempty"`
	Tenants         []string `json:"tenants,omitempty"` // We allow for additional tenants in the future.
}

func (au *Authorizer) isIssuerAllowed(name string) bool {
	for _, re := range au.allowedIssuers {
		if re.MatchString(name) {
			return true
		}
	}
	return false
}

func (au *Authorizer) getOIDCVerifier(ctx context.Context, rawAccessToken string) (Verifier, error) {
	// Parse the token and get the issuer.
	token, err := jwt.ParseJWT(rawAccessToken)
	if err != nil {
		return nil, fmt.Errorf("could not parse JWT: %w", err)
	}
	var claims claims
	if err := json.Unmarshal(token, &claims); err != nil {
		return nil, fmt.Errorf("could not get issuer claim: %w", err)
	}
	issuer := claims.Issuer
	// Check if issuer is allowed.
	if !au.isIssuerAllowed(issuer) {
		return nil, fmt.Errorf("issuer %s not allowed", issuer)
	}
	// We try to guess the client id this token as been issued by.
	clientID := findClientID(claims)
	if clientID == "" {
		logrus.Tracef("%v", string(token))
		return nil, fmt.Errorf("unable to extract client id from access token")
	}
	// See if we already have a verifier.
	au.mu.Lock()
	defer au.mu.Unlock()
	entry, ok := au.verifiers[issuer]
	if ok {
		logrus.Debugf("idp lookup cache hit: %s", issuer)
		return entry.Verifier, nil
	}
	if len(au.verifiers) >= au.maxCacheSize {
		return nil, ErrCacheOverflow
	}
	// Let's fetch a new one.
	provider, err := oidc.NewProvider(ctx, issuer)
	if err != nil {
		return nil, fmt.Errorf("could not create new oidc provider: %w", err)
	}
	// This is where the provider fetches the remote keyset.
	verifier := provider.Verifier(&oidc.Config{ClientID: clientID})
	expireAt := time.Now().Add(time.Hour) // Cache for 1 hour for now.
	au.verifiers[issuer] = cacheEntry{
		Verifier:  verifier,
		ExpiresAt: expireAt,
	}
	return verifier, nil
}

func findClientID(cl claims) string {
	if len(cl.Audience) < 1 && cl.AuthorizedParty != "" {
		return cl.AuthorizedParty
	}
	// If audience is empty and azp too, return empty string.
	if len(cl.Audience) < 1 {
		return ""
	}
	// If only one audience is present, and no azp, return it.
	if len(cl.Audience) == 1 && cl.AuthorizedParty == "" {
		return cl.Audience[0]
	}
	// If azp was present, let's return this.
	if cl.AuthorizedParty != "" {
		return cl.AuthorizedParty
	}
	return "" // Default is to fail.
}

// New returns a new authorizer.
func New(opts ...Opt) (*Authorizer, error) {
	re := new(Authorizer)
	re.verifiers = make(map[string]cacheEntry)
	re.ctx = context.Background()
	re.maxCacheSize = defaultMaxCacheSize

	for _, opt := range opts {
		if err := opt(re); err != nil {
			return nil, err
		}
	}

	// Start goroutine to clean cache regularly.
	t := time.NewTicker(time.Minute * 15)
	go func() {
		for {
			select {
			case <-t.C:
				re.sweepCache()
			case <-re.ctx.Done():
				t.Stop()
				return
			}
		}
	}()

	return re, nil
}

func (au *Authorizer) sweepCache() {
	logrus.Debugf("doing an IdP cache sweep ...")
	au.mu.Lock()
	defer au.mu.Unlock()

	now := time.Now()
	for k, v := range au.verifiers {
		if v.ExpiresAt.Before(now) {
			logrus.Debugf("removing entry %s, expired at %s", k, v.ExpiresAt)
			delete(au.verifiers, k)
		}
	}
}

// Opt configures this authorizer.
type Opt func(*Authorizer) error

// WithHeaderName sets the header name whose content we authorize (tenant).
func WithHeaderName(name string) Opt {
	return func(a *Authorizer) error {
		a.headerName = name
		return nil
	}
}

// WithJWTFromHeader sets the name of the header we need to get the JWT from.
func WithJWTFromHeader(name string) Opt {
	return func(a *Authorizer) error {
		a.jwtFromHeader = name
		return nil
	}
}

// WithClientID sets the client id we look for in the role list we get from the
// token claims.
func WithClientID(id string) Opt {
	return func(a *Authorizer) error {
		a.clientID = id
		return nil
	}
}

// WithAllowedIssuers sets a list of patterns matching allowed issuers in JWT
// tokens sent to us. It is important for security to set this as tight as
// possible. Unless you want to allow any IdP (in the world) ...
//
// If unset no issuer will be allowed.
func WithAllowedIssuers(list []string) Opt {
	return func(a *Authorizer) error {
		for _, v := range list {
			re, err := regexp.Compile(v)
			if err != nil {
				return fmt.Errorf("could not parse issuer pattern %q: %w", v, err)
			}
			a.allowedIssuers = append(a.allowedIssuers, re)
		}
		return nil
	}
}
