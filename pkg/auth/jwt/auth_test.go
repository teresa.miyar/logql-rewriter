package jwt

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"regexp"
	"strings"
	"testing"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"gitlab.com/mironet/logql-rewriter/pkg/jwt"
	"golang.org/x/oauth2"
)

const (
	lokiHeaderName = "X-Scope-OrgID"
	authHeaderName = "Authorization"
)

type jsonTime time.Time

func (j *jsonTime) UnmarshalJSON(b []byte) error {
	var n json.Number
	if err := json.Unmarshal(b, &n); err != nil {
		return err
	}
	var unix int64

	if t, err := n.Int64(); err == nil {
		unix = t
	} else {
		f, err := n.Float64()
		if err != nil {
			return err
		}
		unix = int64(f)
	}
	*j = jsonTime(time.Unix(unix, 0))
	return nil
}

type idToken struct {
	Issuer    string    `json:"iss"`
	Subject   string    `json:"sub"`
	Audience  []string  `json:"aud"`
	Expiry    jsonTime  `json:"exp"`
	IssuedAt  jsonTime  `json:"iat"`
	NotBefore *jsonTime `json:"nbf"`
	Nonce     string    `json:"nonce"`
	AtHash    string    `json:"at_hash"`
}

// mockVerifier returns the parsed id token, but does not really verify it
// against an IdP. This is enough for testing the logic of the Authorizer.
type mockVerifier struct{}

func (m *mockVerifier) Verify(ctx context.Context, rawIDToken string) (*oidc.IDToken, error) {
	rawToken, err := jwt.ParseJWT(rawIDToken)
	if err != nil {
		panic(err)
	}
	var token idToken
	if err := json.Unmarshal(rawToken, &token); err != nil {
		panic(err)
	}
	t := &oidc.IDToken{
		Issuer:   token.Issuer,
		Subject:  token.Subject,
		Audience: token.Audience,
		Expiry:   time.Time(token.Expiry),
		IssuedAt: time.Time(token.IssuedAt),
		Nonce:    token.Nonce, // 🤔
	}
	return t, nil
}

func validRawAccessToken() []byte {
	data, err := ioutil.ReadFile("testdata/valid-token.txt")
	if err != nil {
		panic(err)
	}
	return data
}

func fixtureBytes(name string) []byte {
	data, err := ioutil.ReadFile("testdata/" + name)
	if err != nil {
		panic(err)
	}
	return data
}

func fixtureString(name string) string {
	return string(fixtureBytes(name))
}

func Test_findClientID(t *testing.T) {
	type args struct {
		cl claims
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "no audience claim",
			args: args{
				cl: claims{},
			},
			want: "",
		},
		{
			name: "no audience and one azp claim",
			args: args{
				cl: claims{
					AuthorizedParty: "my-azp-id",
				},
			},
			want: "my-azp-id",
		},
		{
			name: "one audience claim",
			args: args{
				cl: claims{
					Audience: audience{"my-client-id"},
				},
			},
			want: "my-client-id",
		},
		{
			name: "one audience and azp claim",
			args: args{
				cl: claims{
					Audience:        audience{"my-client-id"},
					AuthorizedParty: "my-azp-id",
				},
			},
			want: "my-azp-id",
		},
		{
			name: "several audiences and azp claim",
			args: args{
				cl: claims{
					Audience: audience{
						"my-client-id",
						"my-azp-id",
						"other-identiy",
						"good-service",
						"evil-service",
					},
					AuthorizedParty: "my-azp-id",
				},
			},
			want: "my-azp-id",
		},
		{
			name: "several audiences and no claim",
			args: args{
				cl: claims{
					Audience: audience{
						"my-client-id",
						"my-azp-id",
						"other-identiy",
						"good-service",
						"evil-service",
					},
				},
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findClientID(tt.args.cl); got != tt.want {
				t.Errorf("findClientID() = %v, want %v", got, tt.want)
			}
		})
	}
}

// roundTripperFunc wraps a func so that f is a http.RoundTripper.
type roundTripperFunc func(*http.Request) (*http.Response, error)

func (fn roundTripperFunc) RoundTrip(r *http.Request) (*http.Response, error) {
	return fn(r)
}

func TestAuthorizer_getOIDCVerifier(t *testing.T) {
	var issuerList = []*regexp.Regexp{
		regexp.MustCompile("https://localhost:31000/auth/realms/myrealm"),
	}

	var newClient = func() *http.Client {
		return &http.Client{
			// We mock the transport so we don't have to start a server.
			Transport: roundTripperFunc(func(r *http.Request) (*http.Response, error) {
				dump, err := httputil.DumpRequest(r, true)
				if err != nil {
					panic(err)
				}
				requestURI := r.URL.String()
				t.Logf("request uri: %s", requestURI)
				t.Logf("\n%s\n", string(dump))

				switch {
				case strings.HasSuffix(requestURI, "/.well-known/openid-configuration"):
					// Return the discovery json.
					return &http.Response{
						StatusCode: http.StatusOK,
						Body:       ioutil.NopCloser(bytes.NewBuffer(fixtureBytes("oidc-discovery-response.json"))),
					}, nil
				default:
					t.Errorf("unexpected request")
					return &http.Response{
						StatusCode: http.StatusNotFound,
					}, nil
				}
			}),
		}
	}

	var withValue = func(ctx context.Context, cli *http.Client) context.Context {
		if ctx == nil {
			ctx = context.TODO() // Fresh context for testing for each test.
		}
		return context.WithValue(ctx, oauth2.HTTPClient, cli)
	}

	type wantFunc func(Verifier) error
	type fields struct {
		headerName     string
		jwtFromHeader  string
		clientID       string
		ctx            context.Context
		maxCacheSize   int
		verifiers      map[string]cacheEntry
		allowedIssuers []*regexp.Regexp
	}
	type args struct {
		ctx            context.Context
		rawAccessToken string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    wantFunc
		wantErr bool
	}{
		{
			name: "successful discovery",
			fields: fields{
				headerName:     "logql",
				jwtFromHeader:  "Authorization",
				clientID:       "logql",
				maxCacheSize:   100,
				verifiers:      make(map[string]cacheEntry),
				allowedIssuers: issuerList,
			},
			args: args{
				ctx:            withValue(nil, newClient()),
				rawAccessToken: fixtureString("localToken.txt"),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			au := &Authorizer{
				headerName:     tt.fields.headerName,
				jwtFromHeader:  tt.fields.jwtFromHeader,
				clientID:       tt.fields.clientID,
				ctx:            tt.fields.ctx,
				maxCacheSize:   tt.fields.maxCacheSize,
				verifiers:      tt.fields.verifiers,
				allowedIssuers: tt.fields.allowedIssuers,
			}
			got, err := au.getOIDCVerifier(tt.args.ctx, tt.args.rawAccessToken)
			if (err != nil) != tt.wantErr {
				t.Errorf("Authorizer.getOIDCVerifier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.want != nil {
				if err := tt.want(got); err != nil {
					t.Error(err)
				}
			}
		})
	}
}
