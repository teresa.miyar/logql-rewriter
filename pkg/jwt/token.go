package jwt

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

var (
	// https://grafana.com/docs/loki/latest/api/
	roleMap = map[string]string{
		http.MethodPost:   "logs-write",
		http.MethodDelete: "logs-write",
		http.MethodGet:    "logs-read",
	}
)

func AuthorizeMethod(accessToken []byte, method, clientID string) error {
	var claims struct {
		ResourceAccess map[string]map[string][]string `json:"resource_access"`
	}
	if err := json.Unmarshal(accessToken, &claims); err != nil {
		return fmt.Errorf("could not parse access token: %w", err)
	}
	clientRoleMap, ok := claims.ResourceAccess[clientID]
	if !ok {
		return fmt.Errorf("roles list for client %s missing", clientID)
	}
	clientRoles, ok := clientRoleMap["roles"]
	if !ok {
		return fmt.Errorf("roles list for client %s incomplete", clientID)
	}
	wantRole, ok := roleMap[method]
	if !ok {
		return fmt.Errorf("unexpected http method: %s", method)
	}
	for _, v := range clientRoles {
		if v == wantRole {
			return nil
		}
	}
	return fmt.Errorf("method nod allowed: %s", method)
}

// From go-oidc lib.
func ParseJWT(p string) ([]byte, error) {
	parts := strings.Split(p, ".")
	if len(parts) < 2 {
		return nil, fmt.Errorf("jwt: malformed jwt, expected 3 parts got %d", len(parts))
	}
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, fmt.Errorf("jwt: malformed jwt payload: %v", err)
	}
	return payload, nil
}
