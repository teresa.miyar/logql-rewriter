package main

import (
	"bytes"
	"context"
	"errors"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/logql-rewriter/pkg/auth/basic"
	"golang.org/x/time/rate"
)

type authorizer interface {
	Authorize(ctx context.Context, r *http.Request, limit *rate.Limiter) error
}

var (
	defaultOAuthTimeout      = time.Second * 15
	defaultRateLimitInterval = time.Second / 20
	defaultRateLimitBurst    = 5
)

func newProxy(upstream string, au authorizer) (http.Handler, error) {
	remote, err := url.Parse(upstream)
	if err != nil {
		return nil, err
	}

	errlog := logrus.StandardLogger().WriterLevel(logrus.ErrorLevel)
	proxy := httputil.NewSingleHostReverseProxy(remote)
	proxy.ModifyResponse = func(r *http.Response) error {
		r.Header.Del("Server")
		r.Header.Del("X-Powered-By")
		return nil
	}
	proxy.ErrorLog = log.New(errlog, "proxy: ", 0)
	proxy.ErrorHandler = func(w http.ResponseWriter, r *http.Request, err error) {
		logrus.Error(err)
		http.Error(w, "", http.StatusBadGateway)
	}

	limit := rate.NewLimiter(
		rate.Every(defaultRateLimitInterval),
		defaultRateLimitBurst,
	)

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet && r.Method != http.MethodPost && r.Method != http.MethodDelete {
			http.Error(w, "", http.StatusMethodNotAllowed)
		}
		if logrus.IsLevelEnabled(logrus.TraceLevel) {
			dump, err := httputil.DumpRequest(r, true)
			if err != nil {
				logrus.Errorf("error dumping incoming request: %v", err)
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
			if _, err := io.Copy(os.Stderr, bytes.NewBuffer(dump)); err != nil {
				logrus.Errorf("error dumping incoming request: %v", err)
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
		}

		ctx, cancel := context.WithTimeout(r.Context(), defaultOAuthTimeout)
		defer cancel()
		// Do a shallow copy of the original request.
		r2 := new(http.Request)
		*r2 = *r
		if err := au.Authorize(ctx, r2, limit); err != nil {
			logrus.Error(err)
			if errors.Is(err, basic.ErrTooManyRequests) {
				http.Error(w, http.StatusText(http.StatusTooManyRequests), http.StatusTooManyRequests)
				return
			}
			http.Error(w, "", http.StatusForbidden)
			return
		}
		proxy.ServeHTTP(w, r2)
	}), nil
}
