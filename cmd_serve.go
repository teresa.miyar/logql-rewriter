package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/gorilla/handlers"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mironet/logql-rewriter/pkg/auth/basic"
	"gitlab.com/mironet/logql-rewriter/pkg/auth/jwt"
)

func newServe(pather CommandPather) *cobra.Command {
	v := viper.New()
	initViper(v)

	var (
		removeTokenHeader bool
		provider          *oidc.Provider
	)

	var parseBasicOpts = func() ([]basic.Opt, error) {
		logrus.Infof("using header name '%s' in upstream requests", v.GetString("header"))
		opts := []basic.Opt{
			basic.WithHeaderName(v.GetString("header")),
		}
		opts = append(opts,
			basic.WithOIDCProvider(provider),
			basic.WithOAuth2Config(v.GetString("client-id"), v.GetString("client-secret"), v.GetStringSlice("scope")),
		)
		return opts, nil
	}

	var parseJWTOpts = func() ([]jwt.Opt, error) {
		logrus.Infof("using header name '%s' in upstream requests", v.GetString("header"))
		opts := []jwt.Opt{
			jwt.WithHeaderName(v.GetString("header")),
		}
		logrus.Infof("using IdP %s", provider.Endpoint().TokenURL)
		opts = append(opts,
			jwt.WithClientID(v.GetString("client-id")),
			jwt.WithJWTFromHeader(v.GetString("jwt-from-header")),
			jwt.WithAllowedIssuers(v.GetStringSlice("allowed-idp")),
		)
		return opts, nil
	}

	var cmd = &cobra.Command{
		Use:     "serve",
		Short:   "serve starts the rewriting server.",
		Example: fmt.Sprintf("  %[1]s serve --upstream http://localhost:9090/", pather.CommandPath()),
		RunE: func(cmd *cobra.Command, args []string) error {
			upstream := v.GetString("upstream")
			if upstream == "" {
				return fmt.Errorf("--upstream should be set")
			}
			u, err := url.Parse(upstream)
			if err != nil {
				return err
			}
			if u.Scheme != "https" && u.Scheme != "http" {
				return fmt.Errorf("scheme of %s is not http:// or https://", upstream)
			}
			switch v.GetString("loglevel") {
			case "trace":
				logrus.SetLevel(logrus.TraceLevel)
			case "debug":
				logrus.SetLevel(logrus.DebugLevel)
			case "error":
				logrus.SetLevel(logrus.ErrorLevel)
			default:
				logrus.SetLevel(logrus.InfoLevel)
			}
			logrus.Infof("set log level to %s", logrus.GetLevel())
			// Add basic sanity checks, where the usage help message should be
			// printed on error, before this line. After this line, the usage
			// message is no longer printed on error.
			cmd.SilenceUsage = true

			// Overwrite default rate limits in case they have been set.
			defaultRateLimitInterval = v.GetDuration("idp-rate-limit")
			defaultRateLimitBurst = v.GetInt("idp-rate-burst")

			// Configure IdP.
			provider, err = oidc.NewProvider(context.Background(), v.GetString("idp"))
			if err != nil {
				return err
			}
			logrus.Infof("using IdP %s", provider.Endpoint().TokenURL)

			// Configure proxy to upstream.
			removeTokenHeader = v.GetBool("remove-token-header")
			if removeTokenHeader {
				logrus.Infof("removing auth token headers on upstream calls")
			}
			opts, err := parseBasicOpts()
			if err != nil {
				return err
			}
			basicAuth, err := basic.New(opts...)
			if err != nil {
				return err
			}

			// Configure JWT authorizer.
			jwtOpts, err := parseJWTOpts()
			if err != nil {
				return err
			}
			jwtAuth, err := jwt.New(jwtOpts...)
			if err != nil {
				return err
			}

			// Switching authorizer.
			swi := &switchingAuthorizer{
				basic:              basicAuth,
				jwt:                jwtAuth,
				removeTokenHeaders: []string{authorizationHeaderName},
			}

			proxy, err := newProxy(upstream, swi)
			if err != nil {
				return err
			}
			proxy = handlers.CombinedLoggingHandler(os.Stderr, proxy)
			address := v.GetString("address")
			srv, err := newServer(address, proxy)
			if err != nil {
				return fmt.Errorf("error starting server: %w", err)
			}

			// Start listening for incoming requests.
			errc := make(chan error)
			go func() {
				logrus.Infof("starting server version %s", version)
				logrus.Infof("listening on %s", address)
				logrus.Infof("upstream is %s", upstream)
				errc <- srv.ListenAndServe()
			}()

			term := make(chan os.Signal, 1)
			signal.Notify(term, os.Interrupt, syscall.SIGTERM)

			select {
			case sig := <-term:
				logrus.Infof("received signal %s, terminating", sig)
				_ = srv.Close()
			case err := <-errc:
				if err != http.ErrServerClosed {
					logrus.Errorf("server stopped with error: %v", err)
					return err
				}
			}
			return nil
		},
	}

	// General flags.
	cmd.Flags().String("address", ":8080", "Address where to listen on.")
	cmd.Flags().String("upstream", "", "Upstream we send requests to (Loki).")
	cmd.Flags().String("header", "X-Scope-OrgID", "Header name we use to extract the tenant's name.")
	cmd.Flags().String("loglevel", "", "Log level to use.")

	// These are flags related to the IdP.
	cmd.Flags().String("idp", "", "Base URL of OIDC identity provider.")
	cmd.Flags().Duration("idp-rate-limit", defaultRateLimitInterval, "Allow an IdP lookup every interval duration (for example 200ms).")
	cmd.Flags().Int("idp-rate-burst", defaultRateLimitBurst, "Allow an IdP lookup rate-limit burst.")
	cmd.Flags().StringSlice("scope", []string{"openid", "profile", "email"}, "OIDC scope.")
	cmd.Flags().String("client-id", "", "Client ID on the identity provider.")
	cmd.Flags().String("client-secret", "", "Client secret on the identity provider.")
	cmd.Flags().StringSlice("allowed-idp", nil, "List of regular expressions (RE2) allowing issuers")

	// These are flags related to JWT token authorization.
	//cmd.Flags().Bool("jwt-enabled", true, "Process JWT-based authentication too.")
	cmd.Flags().String("jwt-from-header", "Authorization", "Read JWT from this HTTP header (skip 'Bearer ' if present).")

	// Various other flags follow.
	//cmd.Flags().BoolVar(&flags.insecureSkipVerify, "insecure-skip-verify", false, "Don't verify TLS on upstream ⚠️.")
	cmd.Flags().Bool("remove-token-header", true, "Remove the HTTP header we used to get the JWT from in calls to upstream server.")

	if err := v.BindPFlags(cmd.Flags()); err != nil {
		logrus.Fatal(err)
	}

	return cmd
}
