package main

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
)

const (
	authorizationHeaderName   = "Authorization"
	proxyAuthMethodHeaderName = "X-Proxy-Auth-Method"
	methodBasic               = "basic"
	methodJWT                 = "jwt"
)

type switchingAuthorizer struct {
	basic              authorizer
	jwt                authorizer
	removeTokenHeaders []string
}

// Authorize switches based an authentication method.
func (s switchingAuthorizer) Authorize(ctx context.Context, r *http.Request, lim *rate.Limiter) error {
	var (
		status = statusFailure
		method string
	)
	// Collect duration metrics.
	start := time.Now()
	defer func() {
		d := time.Since(start)
		tokenLookupDuration.WithLabelValues(method, status).Observe(d.Seconds())
	}()

	defer func() {
		// Remove the auth header from the original request if asked to do so.
		for _, v := range s.removeTokenHeaders {
			logrus.Debugf("removing header %s from call to upstream", v)
			delete(r.Header, v)
		}
	}()

	// Check if we use basic auth or JWT auth.
	_, _, ok := r.BasicAuth()
	if ok {
		logrus.Debugf("using basic authentication")
		method = methodBasic
		// Use basic auth.
		r.Header.Set(proxyAuthMethodHeaderName, "basic")
		if err := s.basic.Authorize(ctx, r, lim); err != nil {
			return err
		}
		status = statusSuccess
		return nil
	}

	// Else try JWT auth.
	if bearer := r.Header.Get(authorizationHeaderName); strings.HasPrefix(bearer, "Bearer ") {
		logrus.Debugf("using JWT authentication")
		method = methodJWT
		r.Header.Set(proxyAuthMethodHeaderName, "jwt")
		if err := s.jwt.Authorize(ctx, r, lim); err != nil {
			return err
		}
		status = statusSuccess
		return nil
	}

	return fmt.Errorf("no valid auth method detected")
}
