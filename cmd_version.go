package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	version string = "unversioned"
)

func newVersion(pather CommandPather) *cobra.Command {
	var cmd = &cobra.Command{
		Use:     "version",
		Short:   "Show the version information",
		Example: fmt.Sprintf("  %[1]s version", pather.CommandPath()),
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(version)
		},
	}
	return cmd
}
