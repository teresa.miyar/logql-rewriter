# LogQL Authorization Proxy

This program acts as a reverse proxy in front of [Loki API
instances](https://grafana.com/docs/loki/latest/api/). It is used to authorize
Loki's `X-Scope-OrgID` HTTP header used for
[multitenancy](https://grafana.com/docs/loki/latest/operations/multi-tenancy/).
Since Loki does not have an authentication or authorization feature, this proxy
provides it with the help of an OpenID Connect identity provider (like
[Keycloak](https://www.keycloak.org/)).

![Diagram of where the proxy sits.](docs/diagram.png)

## Mode of Operation

When configured for multitenancy, Loki expects a HTTP header to be present for
all requests, reading or writing. The header might look like this:

```text
X-Scope-OrgID: mytenant
```

A client using _HTTP Basic Authentication_ (like `promtail` does) will be
checked against a set of allowed tenant labels, provided by the IdP in the
OpenID Connect [ID
token](https://www.oauth.com/oauth2-servers/openid-connect/id-tokens/) as
claims, and if allowed will forward the request to its upstream Loki target.

The username and password used during basic authentication will be reused to
fetch a token via the [Resource Owner Password Credentials
Grant](https://tools.ietf.org/html/rfc6749#section-4.3).

The IdP is expected to return a claim named `tenant` with the allowed tenant ID
which will be compared to the `X-Scope-OrgID` sent by the client. It is also
possible to send a list of allowed tenant labels in a claim named `tenants`
(note the plural form).

## Roles

The proxy expects a few roles to be present on the claims in the access tokens,
either sent by the client (e.g. as a bearer token) or issued by the IdP when
doing _Resource Owner Password Credentials Grant_. The roles check if certain
HTTP methods are allowed:

| Role | Allowed HTTP Methods |
| --- | --- |
| `logs-read` | `GET` |
| `logs-write` | `POST`, `DELETE` |

These seem to be all methods used on the [Loki
API](https://grafana.com/docs/loki/latest/api/).

Here an the relevant expected claims in a token:

```json
{
  "resource_access": {
    "logql": {
      "roles": ["logs-write", "logs-read"]
    }
  }
}
```

These claims indicate the requesting user has read and write access to the Loki
API. `logql` is the client id.

## General Security Aspects

The proxy will hash (`sha256`) the username/password combination after use
against the IdP for caching lookups. All tokens are discarded after use. It will
cache the username/password combination until the original token is expired but
will not refresh any tokens automatically since they are not stored.

Lookups sent to the IdP are rate-limited at 20 requests / sec as a default with
5 burstable requests. These values are configurable. The limits are global and
are just a safety net to prevent a DoS attack on the IdP via this proxy.

The mentioned _Resource Owner Password Credentials Grant_ might be [considered
insecure](https://tools.ietf.org/html/rfc6749#section-1.3.3) for most (web)
applications.

> The credentials should only be used when there is a high degree of trust
> between the resource owner and the client.

This here though is an example of the mentioned exceptions where it should be
safe to use this flow: In our case [this example
usage](https://tools.ietf.org/html/rfc6749#section-4.3) applies:

> It is also used to migrate existing clients using direct authentication
> schemes such as HTTP Basic or Digest authentication to OAuth by converting the
> stored credentials to an access token.

## Instrumentation

Prometheus metrics are exposed on `/metrics`. Currently the standard Go metrics
are exposed plus an additional histogram measuring IdP latencies.

The server can be monitored by issuing health checks to `/healthz`.

## Contributing

Use `make start-keycloak` to start a Keycloak server preconfigured with two
realms, the `master` realm and the `myrealm` realm. In the `myrealm` realm
there's one user and a client (OIDC) preconfigured:

After starting Keycloak start the app by sourcing the provided environment
variables:

```bash
make build
make start-keycloak
# This also fetches a fresh access token stored in $TOKEN.
source contrib/localenv/client.env.sh
./logql-rewriter serve --loglevel debug --remove-token-header
```

Test with:

```bash
# Basic Auth
curl -v localhost:8080/anything -H 'X-Scope-OrgID: myrealm' -u myuser:test
curl -v localhost:8080/anything -X POST -H 'X-Scope-OrgID: myrealm' -u myuser:test
curl -v localhost:8080/anything -H 'X-Scope-OrgID: myrealm' -u readuser:test
curl -v localhost:8080/anything -X POST -H 'X-Scope-OrgID: myrealm' -u readuser:test
# JWT Auth (access token)
curl -v localhost:8080/anything -H 'X-Scope-OrgID: myrealm' -H "Authorization: Bearer $TOKEN"
curl -v localhost:8080/anything -X POST -H 'X-Scope-OrgID: myrealm' -H "Authorization: Bearer $TOKEN"
```

The credentials to the Keycloak `master` realm are `admin:admin`.
